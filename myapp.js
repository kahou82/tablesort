var myapp = angular.module("myapp", [])

myapp.controller("tableController", ["$scope", 
	function($scope) {
		$scope.item = "hi";

		$scope.subnets = 
			[{host: "host1", ip: "3.1.1.1", status: "status", state: "state10", detail: "detail"},
			 {host: "vlan host", ip: "20.1.1.1", status: "status1", state: "state1", detail: "detail1"},
			 {host: "L2 host", ip: "122.1.1.1", status: "status1", state: "state1", detail: "detail1"},
			 {host: "L3 host", ip: "23.1.1.1", status: "status1", state: "state1", detail: "detail1"},
			 {host: "host5", ip: "99.1.1.1", status: "status1", state: "state1", detail: "detail1"},
			 {host: "host99", ip: "90.1.1.1", status: "status1", state: "state1", detail: "detail1"},
			 {host: "my host", ip: "91.1.1.1", status: "status1", state: "state1", detail: "detail1"},
			 {host: "your host2", ip: "92.1.1.1", status: "status1", state: "state1", detail: "detail1"},
			 {host: "host", ip: "95.1.1.1", status: "status1", state: "state1", detail: "detail1"},
			 {host: "basic", ip: "96.1.1.1", status: "status1", state: "state1", detail: "detail1"},
			 {host: "testing", ip: "97.1.1.1", status: "status1", state: "state1", detail: "detail1"}]
		
		$scope.expand_list = [];

		$scope.expand = function($event) {
			var cur_cell = $event.target;
			var cur_row = cur_cell.parentNode;

			for (var i = 0; i < $scope.expand_list.length; i++) 
			{
				if ($scope.expand_list[i] == cur_row) {
					return
				}
			}

			var cur_table = cur_row.parentNode;
			$("<tr><td></td><td></td><td>Detail Data</td><td></td><td></td></tr>").insertAfter(cur_row);
			$scope.expand_list.push(cur_row);
		}
	}
]);